import { LightningElement, api } from 'lwc';

export default class CustomPathInnerLwc extends LightningElement {
    @api selectedValue;
    @api value
    @api label
    get classList() {
        return this.value === this.selectedValue ? 'slds-path__item slds-is-current slds-is-active' : 'slds-path__item slds-is-incomplete';
    }
}