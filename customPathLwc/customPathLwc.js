// Import LightningElement and api classes from lwc module
import { LightningElement, api, wire } from 'lwc';
// import getPicklistValues method from lightning/uiObjectInfoApi
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
// import getObjectInfo method from lightning/uiObjectInfoApi
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
// Import lead object APi from schema
import LEAD_OBJECT from '@salesforce/schema/Lead';
// import Lead status field from schema
import PICKLIST_FIELD from '@salesforce/schema/Lead.Status';

export default class CustomPathLwc extends LightningElement {
    @api picklistFieldName;
    

    @wire(getObjectInfo, { objectApiName: LEAD_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: PICKLIST_FIELD })
    picklistFieldValues;

}